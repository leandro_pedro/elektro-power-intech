VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Tag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'******************************************************************************'
' Project   : Ingeteam / Neoenergia
' File      : Tag.cls
' Author    : leandro.pedro@intech-automacao.com.br
' Date      : 2020/10/19
'
' Revision history
'------------------------------------------------------------------------------'
' Date      Author  Revision
' 20/10/19  lbp     First commit.
' 20/10/21  lbp     Upd AdviseType aquisicao e slave p/ AlwaysInAdvise
'                   Add FormatDocString
' 20/11/06  lbp     Fix N1/N2 for slave + Add EnableDeadband.
'                   Rem caracter 186 (ordinal);
'                   Fix tempo de abertura.
' 22/06/24  lbp     Upd funcao FormatDocString()
'------------------------------------------------------------------------------''
' Copyright (c) 2020 by In-Tech Automacao & Sistemas. All Rights Reserved.
'******************************************************************************'
Option Explicit

Enum AdviseTypes
    AlwaysInAdvise = 0
    AdviseWhenLinked = 1
End Enum

Private m_ObjectType As String
Private m_Ref As String
Private m_Name As String


' Identificacao
Public DocString As String
'Public Name As String

Public N4 As Integer

'
'
'
Public Sub Init(ByVal ObjectType As String, _
                ByVal TagName As String, _
                ByVal sDocString As String, _
                ByVal Ref As String)

    m_ObjectType = ObjectType
    m_Name = TagName
    DocString = FormatDocString(sDocString)
    m_Ref = Ref
End Sub
'
'
'
Public Property Get ObjectFunction()
    Select Case True
        Case m_ObjectType Like "SIMPLE?": ObjectFunction = "DS"
        Case m_ObjectType Like "DUPLO": ObjectFunction = "DD"
        Case m_ObjectType Like "ANAL?GIC*": ObjectFunction = "AN"
        Case m_ObjectType Like "COMANDO*": ObjectFunction = "CD"
        Case m_ObjectType Like "PAR?METRO*": ObjectFunction = "SP"
    End Select
End Property
'
'
'
Public Property Get Name()
    Select Case ObjectFunction
        Case "DS", "DD":    Name = "Digitais.[" & m_Name & "]"
        Case "AN":          Name = "Analogicos.[" & m_Name & "]"
        Case "CD":          Name = "Comandos.[" & m_Name & "]"
        Case "SP":          Name = "Setpoints.[" & m_Name & "]"
    End Select
End Property
'
'
'
Public Property Get ObjectType()
    ObjectType = "IOTag"
End Property
'
'
'
Public Property Get IEC61850() As VBA.Collection
    Set IEC61850 = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            IEC61850.Add Item:="True", Key:="AllowRead"
            IEC61850.Add Item:="False", Key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            IEC61850.Add Item:="False", Key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "DD"
            IEC61850.Add Item:="True", Key:="AllowRead"
            IEC61850.Add Item:="False", Key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            IEC61850.Add Item:="False", Key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "AN"
            IEC61850.Add Item:="True", Key:="AllowRead"
            IEC61850.Add Item:="False", Key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            IEC61850.Add Item:="False", Key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"

        Case "CD", "SP"
            IEC61850.Add Item:="False", Key:="AllowRead"
            IEC61850.Add Item:="True", Key:="AllowWrite"
            IEC61850.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            IEC61850.Add Item:="False", Key:="EnableDriverEvent"
            ' IEC61850.Add Item:=1, key:="EnableDeadBand"
    End Select

    IEC61850.Add Item:=m_ParamItem, Key:="ParamItem"
    IEC61850.Add Item:=m_ParamDevice, Key:="ParamDevice"
End Property
'
'
'
Public Property Get Master() As VBA.Collection
    Set Master = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            Master.Add Item:=1, Key:="N1"
            Master.Add Item:=1, Key:="N2"
            Master.Add Item:=202, Key:="N3"
            Master.Add Item:="True", Key:="AllowRead"
            Master.Add Item:="False", Key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, Key:="AdviseType"
            Master.Add Item:="False", Key:="EnableDriverEvent"
            Master.Add Item:="False", Key:="EnableDeadBand"

        Case "DD"
            Master.Add Item:=1, Key:="N1"
            Master.Add Item:=1, Key:="N2"
            Master.Add Item:=402, Key:="N3"
            Master.Add Item:="True", Key:="AllowRead"
            Master.Add Item:="False", Key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, Key:="AdviseType"
            Master.Add Item:="False", Key:="EnableDriverEvent"
            Master.Add Item:="False", Key:="EnableDeadBand"

        Case "AN"
            Master.Add Item:=1, Key:="N1"
            Master.Add Item:=1, Key:="N2"
            Master.Add Item:=3205, Key:="N3"
            Master.Add Item:="True", Key:="AllowRead"
            Master.Add Item:="False", Key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, Key:="AdviseType"
            Master.Add Item:="False", Key:="EnableDriverEvent"
            Master.Add Item:="False", Key:="EnableDeadBand"

        Case "CD"
            Master.Add Item:=1, Key:="N1"
            Master.Add Item:=5, Key:="N2"
            Master.Add Item:=1201, Key:="N3"
            Master.Add Item:="False", Key:="AllowRead"
            Master.Add Item:="True", Key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, Key:="AdviseType"
            Master.Add Item:="False", Key:="EnableDriverEvent"
            Master.Add Item:="False", Key:="EnableDeadBand"

        Case "SP"
            Master.Add Item:=1, Key:="N1"
            Master.Add Item:=5, Key:="N2"
            Master.Add Item:=4103, Key:="N3"
            Master.Add Item:="False", Key:="AllowRead"
            Master.Add Item:="True", Key:="AllowWrite"
            Master.Add Item:=AdviseWhenLinked, Key:="AdviseType"
            Master.Add Item:="False", Key:="EnableDriverEvent"
            Master.Add Item:="False", Key:="EnableDeadBand"
    End Select
End Property
'
'
'
Public Property Get Slave() As VBA.Collection
    Set Slave = New VBA.Collection
    Select Case ObjectFunction
        Case "DS"
            Slave.Add Item:=21, Key:="N1"
            Slave.Add Item:=1, Key:="N2"
            Slave.Add Item:=202, Key:="N3"
            Slave.Add Item:="False", Key:="AllowRead"
            Slave.Add Item:="True", Key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            Slave.Add Item:="False", Key:="EnableDriverEvent"
            Slave.Add Item:="False", Key:="EnableDeadBand"

        Case "DD"
            Slave.Add Item:=21, Key:="N1"
            Slave.Add Item:=1, Key:="N2"
            Slave.Add Item:=402, Key:="N3"
            Slave.Add Item:="False", Key:="AllowRead"
            Slave.Add Item:="True", Key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            Slave.Add Item:="False", Key:="EnableDriverEvent"
            Slave.Add Item:="False", Key:="EnableDeadBand"

        Case "AN"
            Slave.Add Item:=12, Key:="N1"
            Slave.Add Item:=1, Key:="N2"
            Slave.Add Item:=3205, Key:="N3"
            Slave.Add Item:="False", Key:="AllowRead"
            Slave.Add Item:="True", Key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            Slave.Add Item:="False", Key:="EnableDriverEvent"
            Slave.Add Item:="False", Key:="EnableDeadBand"

        Case "CD"
            Slave.Add Item:=12, Key:="N1"
            Slave.Add Item:=5, Key:="N2"
            Slave.Add Item:=1201, Key:="N3"
            Slave.Add Item:="True", Key:="AllowRead"
            Slave.Add Item:="False", Key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            Slave.Add Item:="True", Key:="EnableDriverEvent"
            Slave.Add Item:="False", Key:="EnableDeadBand"

        Case "SP"
            Slave.Add Item:=12, Key:="N1"
            Slave.Add Item:=5, Key:="N2"
            Slave.Add Item:=4103, Key:="N3"
            Slave.Add Item:="True", Key:="AllowRead"
            Slave.Add Item:="False", Key:="AllowWrite"
            Slave.Add Item:=AlwaysInAdvise, Key:="AdviseType"
            Slave.Add Item:="True", Key:="EnableDriverEvent"
            Slave.Add Item:="False", Key:="EnableDeadBand"
    End Select
End Property
'
'
'
Private Property Get m_ParamItem()
    Dim arr As Variant
    Dim s As String
    s = m_Ref & ".#.#"
    arr = Split(Replace(s, "/", "."), ".")
    If UBound(arr) > 5 Then
        ' Formata referencia 61850 para o driver do elipse.
        If (ObjectFunction = "AN") Then
            If (UBound(arr) > 6) Then
                If (arr(3) = "OpCnt") Then
                    s = arr(2) & "$ST$OpCnt$stVal"
                Else
                    s = arr(2) & "$" & arr(5) & "$" & arr(3) & "$" & arr(4) & "$cVal$mag$f"
                End If
            'ElseIf (UBound(arr) = 6) Then
            Else
                If (arr(3) = "OpTmms") Then
                    ' Tempo de abertura
                    s = arr(2) & "$ST$OpTmms$stVal"
                Else
                    s = arr(2) & "$" & arr(4) & "$" & arr(3) & "$mag$f"
                End If
            End If
        Else
            If (arr(4) = "#") Then
                arr(4) = "ST"
            ElseIf (arr(5) = "#") Then
                arr(5) = GetAttribute(arr(3))
            End If
            
            s = arr(2) & "$" & arr(4) & "$" & arr(3) & "$" & arr(5)
        End If
        
        m_ParamItem = s
        
    End If
End Property
'
'
'
Private Property Get m_ParamDevice()
    Dim arr As Variant
    arr = Split(Replace(m_Ref, "/", "."), ".")
    m_ParamDevice = arr(0) & ":" & arr(0) & arr(1)
End Property
'
'
'
Private Function GetAttribute(ByVal s As String) As String
    With CreateObject("VBScript.RegExp")
        .Global = True
        .MultiLine = False
        .IgnoreCase = False
        .Pattern = "^(Op|TrDev.*[0-9]|Tr|OpCls|RecOK|RestST|RecCyc.*[0-9]|DefTrip)$"
         GetAttribute = IIf(.Test(s), "general", "stVal")
    End With
End Function
'
' Remove acento da DocString
'
Private Function FormatDocString(ByRef Text As String)
    Const sFind = "�����������������������������������������"
    Const sReplace = "AAAAACEEEEIIIINOOOUUUaaaaceeeeiiiinooouuu"
    Dim i As Integer
    Dim iPos As Integer
    For i = 1 To Len(Text)
        iPos = InStr(sFind, Mid(Text, i, 1))
        If (iPos > 0) Then
            Mid(Text, i, 1) = Mid(sReplace, iPos, 1)
        End If
    Next
    FormatDocString = Text
End Function

