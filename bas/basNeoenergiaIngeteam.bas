Attribute VB_Name = "basNeoenergiaIngeteam"
'******************************************************************************'
' Project   : Ingeteam / Neoenergia
' File      : basNeoenergiaIngeteam.bas
' Author    : leandro.pedro@intech-automacao.com.br
' Date      : 2020/10/19
'
' Revision history
'------------------------------------------------------------------------------'
' Date      Author  Revision
' 20/10/19  lbp     First commit.
' 20/10/20  lbp     Add AuxGateway + SisInfo.
' 20/10/21  lbp     Rem filtro da planilha e tags tachado.
' 20/11/06  lbp     Fix master SinInfo + Add EnableDeadband.
' 21/10/21  lbp     Add input endereco DNP3.
' 21/12/20  lbp     Add dict para mensagem de aviso de duplicatas.
'                   Upd encoding do .csv.
' 22/06/24  lbp     Add enderecamento N4 pela coluna 'INDEX DNP'.
'                   Upd desenvolvido a partir de Ceilandia SUL.
'------------------------------------------------------------------------------'
' Copyright (c) 2020 by In-Tech Automacao & Sistemas. All Rights Reserved.
'******************************************************************************'

Option Explicit

Public g_Dict As Object

Enum IODrivers
    IEC61850 = 0
    DNP3Master = 1
    DNP3Slave = 2
    AuxGateway = 3
End Enum
'
'
'
Public Sub Xls2Elipse()
    Call Main
End Sub
'
'
'
Public Sub Xls2ElipseOnlyModified()
    If (MsgBox("Tem certeza que deseja gerar somente os pontos" & _
                 " em AMARELO?", vbQuestion + vbYesNo, "Aten��o") = vbYes) Then
        Call Main(True)
    End If
End Sub

Private Sub Main(Optional ByVal OnlyModified As Boolean = False)
    Dim bIsColumnIndexDnpExist As Boolean
    Dim bIsIncludedOnDatabase As Boolean
    Dim i As Long
    Dim iColDnpIndex As Integer
    Dim iColDocstring As Integer
    Dim iColIED As Integer
    Dim iColIncludedOnDatabase As Integer
    Dim iColRef As Integer
    Dim iColTag As Integer
    Dim iColType As Integer
    Dim iLastRow As Long
    Dim lAddress As Integer
    Dim listAuxGateway As Object
    Dim listDNP3Master As Object
    Dim listDNP3Slave As Object
    Dim listIEC61850 As Object
    Dim listSisInfo As Object
    Dim lStartAddress As Long
    Dim objWs As Worksheet
    Dim oDict As Object
    Dim oTag As Tag
    Dim sDocString As String
    Dim sIED As String
    Dim sRef As String
    Dim sTag As String
    Dim sType As String
    Dim sWarningMessage As String

    Set listIEC61850 = CreateObject("System.Collections.ArrayList")
    Set listDNP3Master = CreateObject("System.Collections.ArrayList")
    Set listDNP3Slave = CreateObject("System.Collections.ArrayList")
    Set listAuxGateway = CreateObject("System.Collections.ArrayList")
    Set listSisInfo = CreateObject("System.Collections.ArrayList")
    
    Set oDict = CreateObject("Scripting.Dictionary")
    
    ' Procura em todas as planilhas pela coluna 'INDEX DNP'.
    For Each objWs In ThisWorkbook.Sheets
        If (GetColumnNumberByName(objWs, "INDEX DNP") > 0) Then
            bIsColumnIndexDnpExist = True
            Exit For
        End If
    Next objWs
    
    ' Se a coluna 'INDEX DNP' nao estiver presente, habilita a opcao
    ' de escolha do index incial, default = 0.
    If (Not bIsColumnIndexDnpExist) Then
        ' Endreco N4 do driver DNP3:
        ' -  3000~ 3099 reservada aos automatismos.
        ' - 10000~10999 reservada aos tags de qualidade de comunicacao.
        Do
            lStartAddress = InputBox("Deseja definir um endere�o inicial de configura��o?", , 0)
        Loop Until IsNumeric(lStartAddress) And _
                    Not (lStartAddress >= 3000 And lStartAddress <= 3999) And _
                    Not (lStartAddress >= 10000 And lStartAddress <= 10999)
        
        lAddress = CLng(lStartAddress)
    End If


    For Each objWs In ThisWorkbook.Sheets
        ' Clear active filter.
        If (objWs.FilterMode) Then
            objWs.ShowAllData
        End If
        
        iColIncludedOnDatabase = GetColumnNumberByName(objWs, "INCLUIDO")
        iColType = GetColumnNumberByName(objWs, "TIPO")
        iColIED = GetColumnNumberByName(objWs, "IED", xlWhole)
        iColTag = GetColumnNumberByName(objWs, "TAG E3")
        iColDocstring = GetColumnNumberByName(objWs, "DESC")
        iColRef = GetColumnNumberByName(objWs, "REFE")
        iColDnpIndex = GetColumnNumberByName(objWs, "INDEX DNP")

        If (Not bIsColumnIndexDnpExist) Then
            lAddress = lStartAddress
        End If

        iLastRow = objWs.Cells.SpecialCells(xlCellTypeLastCell).Row

        For i = 1 To iLastRow
            ' Verifica se ponto esta 'incluido na base de dados' de acordo com o preenchimento
            ' da coluna "INCLUIDO B.D.", e.g.: SIM|S|X
            Select Case UCase(Trim(objWs.Cells(i, iColIncludedOnDatabase).Value))
                Case "X", "S", "SIM"
                    If (objWs.Cells(i, iColIncludedOnDatabase).Characters.Font.Strikethrough) Then
                        ' Tachado
                        bIsIncludedOnDatabase = False
                    Else
                        If (OnlyModified) Then
                            bIsIncludedOnDatabase = _
                                    (objWs.Cells(i, iColIncludedOnDatabase).Interior.Color = vbYellow)
                        Else
                            bIsIncludedOnDatabase = True
                        End If
                    End If
                Case Else
                    bIsIncludedOnDatabase = False
            End Select
            
            sType = UCase(Trim(objWs.Cells(i, iColType).Value))
            sIED = UCase(Trim(objWs.Cells(i, iColIED).Value))
            sTag = UCase(Trim(objWs.Cells(i, iColTag).Value))
            sDocString = UCase(Trim(objWs.Cells(i, iColDocstring).Value))
            sRef = Trim(objWs.Cells(i, iColRef).Value)
            
            
            ' Confirma se ponto esta incluido na base e nenhum dos parametros mandatorios estao vazios.
            If (bIsIncludedOnDatabase) And (Not IsThereAnEmptyString(sType, sTag, sRef)) Then
                
                ' SisInfo
                If (Not listSisInfo.Contains(sIED)) Then
                    listSisInfo.Add sIED
                End If
                
                ' Tag
                Set oTag = New Tag
                
                Call oTag.Init(sType, sTag, sDocString, sRef)
                
                ' Aviso em caso de ponto duplicado.
                If (oDict.Exists(oTag.Name)) Then
                    sWarningMessage = sWarningMessage & "- " & oTag.Name & vbCrLf
                Else
                    oDict.Add oTag.Name, Nothing
                End If


                If (bIsColumnIndexDnpExist) Then
                    ' Endereco proposto na coluna 'INDEX DPN'.
                    lAddress = CLng(objWs.Cells(i, iColDnpIndex).Value)
                End If
                
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' IEC61850
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem
                listIEC61850.Add Join(Array(oTag.ObjectType, _
                                            oTag.IEC61850.Item("AdviseType"), _
                                            oTag.Name, _
                                            oTag.IEC61850.Item("AllowRead"), _
                                            oTag.IEC61850.Item("AllowWrite"), _
                                            oTag.DocString, _
                                            oTag.IEC61850.Item("ParamDevice"), _
                                            oTag.IEC61850.Item("ParamItem") _
                                        ), ";")
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' DNP3Master
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4
                listDNP3Master.Add Join(Array( _
                                            oTag.ObjectType, _
                                            oTag.Master.Item("AdviseType"), _
                                            oTag.Name, _
                                            oTag.Master.Item("AllowRead"), _
                                            oTag.Master.Item("AllowWrite"), _
                                            oTag.DocString, _
                                            oTag.Master.Item("EnableDeadband"), _
                                            oTag.Master.Item("N1"), _
                                            oTag.Master.Item("N2"), _
                                            oTag.Master.Item("N3"), _
                                            lAddress _
                                        ), ";")
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' DNP3Slave
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source
                listDNP3Slave.Add Join(Array( _
                                            oTag.ObjectType, _
                                            oTag.Slave.Item("AdviseType"), _
                                            oTag.Name, _
                                            oTag.Slave.Item("AllowRead"), _
                                            oTag.Slave.Item("AllowWrite"), _
                                            oTag.DocString, _
                                            oTag.Slave.Item("N1"), _
                                            oTag.Slave.Item("N2"), _
                                            oTag.Slave.Item("N3"), _
                                            lAddress, _
                                            oTag.Slave.Item("EnableDeadband"), _
                                            oTag.Slave.Item("EnableDriverEvent") _
                                        ), ";")
                
                If (oTag.ObjectFunction <> "CD") Then
                    ' Associacao no slave exceto comandos
                    listDNP3Slave.Add "AgSimple;;" & oTag.Name & ".Links.Value;;;;;;;;;;Aquisicao.[61850].IEC61850." & oTag.Name & ".Value"
                Else
                    ' Lista de comandos p/ AuxGateway
                    ' ObjectType;Name;DocString;InputTag;OutputTag;CloseOutputValue;TripOutputValue
                    listAuxGateway.Add "xoComandoGenerico;" & sTag & ";" & oTag.DocString & _
                                        ";Distribuicao.DNP3Slave.Comandos." & sTag & _
                                        ";Aquisicao.[61850].IEC61850.Comandos." & sTag & _
                                        ";1;0"
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If (Not bIsColumnIndexDnpExist) Then
                    lAddress = lAddress + 1
                End If
            End If
        Next
    Next
    
    ' IEC61850
    BuildListHeader listIEC61850, IEC61850
    MergeSisInfoIntoDriverList listSisInfo, listIEC61850, IEC61850
    ExportToCsv listIEC61850, ThisWorkbook.Path & "\IEC61850.csv"

    ' DNP3Master
    BuildListHeader listDNP3Master, DNP3Master
    MergeSisInfoIntoDriverList listSisInfo, listDNP3Master, DNP3Master
    ExportToCsv listDNP3Master, ThisWorkbook.Path & "\DNP3Master.csv"

    ' DNP3Slave
    BuildListHeader listDNP3Slave, DNP3Slave
    MergeSisInfoIntoDriverList listSisInfo, listDNP3Slave, DNP3Slave
    ExportToCsv listDNP3Slave, ThisWorkbook.Path & "\DNP3Slave.csv"
    
    ' AuxGateway
    BuildListHeader listAuxGateway, AuxGateway
    ExportToCsv listAuxGateway, ThisWorkbook.Path & "\AuxGateway.csv"
    
    CopyToClipboard ThisWorkbook.Path
    
    If (sWarningMessage <> "") Then
        MsgBox "Os Tags a seguir aparecem duplicados na lista de pontos:" & _
                vbCrLf & vbCrLf & sWarningMessage, vbExclamation, "Aten��o"
    End If
    
    MsgBox "Fim", vbInformation
End Sub
'
'
'
Private Function GetColumnNumberByName(ByVal Sheet As Worksheet, _
                                        ByVal ColumnName As String, _
                                        Optional LookAt As XlLookAt = xlPart) As Integer
    Dim r As Range
    Set r = Sheet.Cells.EntireRow.Find(What:=ColumnName _
                                        , LookIn:=xlValues _
                                        , LookAt:=LookAt _
                                        , SearchOrder:=xlByColumns _
                                        , SearchDirection:=xlPrevious _
                                        , MatchCase:=False)
    
    If (r Is Nothing) Then
        ' If r isn't something (prevent errors).
        GetColumnNumberByName = 0
    Else
        ' Set to the first time a match is found.
        GetColumnNumberByName = r.Column
    End If
End Function
'
'
'
Private Function IsThereAnEmptyString(ParamArray args() As Variant) As Boolean
    Dim arg As Variant
    Dim bRetval As Boolean: bRetval = False
    For Each arg In args
        If (VarType(arg) <> vbString) Then
            bRetval = True
            Exit For
        Else
            If (Len(arg) = 0) Then
                bRetval = True
                Exit For
            End If
        End If
    Next
    IsThereAnEmptyString = bRetval
End Function
'
'
'
Private Sub ExportToCsv(ByVal ArrayList As Object, ByVal FilePath As String)
    Dim sElem As Variant
    Dim oStream As Object
    
    With CreateObject("ADODB.Stream")
        .Type = 2
        .Charset = "iso-8859-1"
        .Open
        
        For Each sElem In ArrayList
            .WriteText sElem + vbCrLf
        Next

        Set oStream = CreateObject("ADODB.Stream")
        oStream.Type = 1
        oStream.Mode = 3
        oStream.Open
        
        .Position = 0
        .CopyTo oStream
        .Flush
        .Close
        
        oStream.SaveToFile FilePath, 2
        oStream.Close
    End With
End Sub
'
'
'
Private Sub CopyToClipboard(ByVal Text As String)
    Dim obj As Object
    
    On Error Resume Next
    
    Set obj = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    obj.SetText Text
    obj.PutInClipboard
    Set obj = Nothing
    
    On Error GoTo 0
End Sub
'
'
'
Private Sub BuildListHeader(ByRef ArrayList As Object, ByVal IODriver As IODrivers)
    Dim objWs As Worksheet
    Dim i As Integer
    ' Adiciona cabecalho
    With ArrayList
        Select Case IODriver
            Case IODrivers.IEC61850
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem"
            Case IODrivers.DNP3Master
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4"
            Case IODrivers.DNP3Slave
                .Insert 0, "ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source"
            Case IODrivers.AuxGateway
                .Insert 0, "ObjectType;Name;DocString;InputTag;OutputTag;CloseOutputValue;TripOutputValue"
                
                Exit Sub    ' <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        End Select
        

        .Insert 1, "IOFolder;;Digitais"
        .Insert 2, "IOFolder;;Analogicos"
        .Insert 3, "IOFolder;;Comandos"
        .Insert 4, "IOFolder;;SisInfo"

        For Each objWs In ThisWorkbook.Sheets
            If (objWs.Name Like "PAR?METRO*") Then
                .Insert 5, "IOFolder;;Setpoints"
            End If
        Next
    End With
End Sub
'
'
'
Private Sub MergeSisInfoIntoDriverList(ByVal SisInfoList As Object _
                                    , ByRef DriverList As Object _
                                    , ByVal IODriver As IODrivers)
    Dim elem As Variant
    Dim lAddress As Integer
    
    ' offset
    lAddress = 10000
    For Each elem In SisInfoList
        Select Case IODriver
            Case IODrivers.IEC61850
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;ParamDevice;ParamItem
                DriverList.Add "IOTag;0;SisInfo." & elem & _
                        ";True;False;STATUS COMUNICACAO;" & elem & ";ServerStatus"
            Case IODrivers.DNP3Master
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;EnableDeadband;N1;N2;N3;N4
                DriverList.Add "IOTag;1;SisInfo." & elem & _
                        ";True;False;STATUS COMUNICACAO;False;1;1;202;" & lAddress
            Case IODrivers.DNP3Slave
                ' ObjectType;AdviseType;Name;AllowRead;AllowWrite;DocString;N1;N2;N3;N4;EnableDeadband;EnableDriverEvent;Source
                DriverList.Add "IOTag;0;SisInfo." & elem & _
                        ";False;True;STATUS COMUNICACAO;21;1;202;" & lAddress & ";False;False"
                DriverList.Add "AgSimple;;SisInfo." & elem & _
                        ".Links.Value;;;;;;;;;;Abs(Aquisicao.[61850].IEC61850.SisInfo.[" & elem & "].Value <> 5)"
        End Select
        lAddress = lAddress + 1
    Next
End Sub

